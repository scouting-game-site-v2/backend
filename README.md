# Scouting Game Site V2 - Backend
The backend for the scouting game site v2. This is a rewrite of the original scouting game site backend, which was written in Pyhton (Flask). This version is written in Go (Golang) and wil use a PostgreSQL database.

### Entrypoints for accounts:
- `/api/account` - (GET) Admin panel for accounts
- `/api/account` - (POST) Register a new account
- `/api/account/{ID}` - (GET) Info of an account
- `/api/account/{ID}` - (PUT) Update an account 


## Installation
### Programs
- Go (Golang)
- PostgreSQL

## Environment variables
- JWT_SECRET - The secret key used for JWT
- POSTGRES_PASSWORD - The password for the PostgreSQL database

### Set environment variables
#### Linux
```bash
export JWT_SECRET=my_secret_key
export POSTGRES_PASSWORD=test
export POSTGRES_HOST=localhost
```
for deployment:
```bash
export POSTGRES_HOST=postgres
```

#### Windows
```powershell
$JWT_SECRET = "my_secret_key"
$POSTGRES_PASSWORD = "test"
$POSTGRES_HOST = "localhost"
```

### Go modules installation
```bash
go get github.com/gorilla/mux
go get github.com/lib/pq
go get -u github.com/golang-jwt/jwt/v4
go get golang.org/x/crypto/bcrypt
go get github.com/stretchr/testify
```

### Steps
1. Create a PostgreSQL database
```powershell	
docker run --name "ScoutingGameSiteV2-DataBase" -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -p 5432:5432 -d postgres
```

2. On the first run, seed the database
```bash
go run . -seed
```

3. Run the backend
```bash
go run main.go
```

## Docker run
```powershell
docker run --name "ScoutingGameSiteV2-backend" -e POSTGRES_PASSWORD=$POSTGRES_PASSWORD -e JWT_SECRET=$JWT_SECRET -p 8080:8080 -d backend
```