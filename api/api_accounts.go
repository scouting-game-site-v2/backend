package api

import (
	"ScoutingGameSiteV2_Backend/common"
	"ScoutingGameSiteV2_Backend/logger"
	"encoding/json"
	"net/http"
	"os"
	"strings"

	"golang.org/x/crypto/bcrypt"

	jwt "github.com/golang-jwt/jwt/v4"
	"github.com/gorilla/mux"
)

// Hande a request to /login
func (s *APIServer) handleLogin(w http.ResponseWriter, r *http.Request) error {
	logger.Debug("Handling login")
	// Decode the request body into a LoginRequest struct
	var loginReq LoginRequest
	if err := json.NewDecoder(r.Body).Decode(&loginReq); err != nil {
		return WriteJSON(w, http.StatusBadRequest, errBadRequest)
	}

	// Get the account from the database
	acc, err := s.accountStore.GetAccountByUsername(loginReq.Username)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleLogin at operation GetAccountByUsername:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	// Check if the password is valid
	if !acc.ValidPassword(loginReq.Password) {
		return WriteJSON(w, http.StatusForbidden, errInvalidPassword)
	}

	// Generate a JWT token
	token, err := generateJWT(acc)
	if err != nil {
		logger.Error("Error in handleLogin at operation generateJWT:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	// Write the response
	resp := LoginResponse{
		Token:   token,
		Account: acc,
	}

	return WriteJSON(w, http.StatusOK, resp)
}

// Retrieve all accounts
func (s *APIServer) handleGetAccounts(w http.ResponseWriter, r *http.Request) error {
	logger.Debug("Handling get all accounts")
	// Retrieve the requesting user
	user, err := s.getAccountByToken(w, r)
	if err != nil {
		return WriteJSON(w, http.StatusUnauthorized, errUnauthorized)
	}

	// Check if the user is an admin
	if !user.Admin {
		return WriteJSON(w, http.StatusForbidden, errForbidden)
	}

	accounts, err := s.accountStore.GetAccounts()
	if err != nil {
		logger.Error("Error in handleGetAccounts at operation GetAccounts:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	return WriteJSON(w, http.StatusOK, accounts)
}

// Create a new account
func (s *APIServer) handleCreateAccount(w http.ResponseWriter, r *http.Request) error {
	logger.Debug("Handling create new account")
	// Decode the request body into a CreateAccountRequest struct
	req := new(CreateAccountRequest)
	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		return WriteJSON(w, http.StatusBadRequest, errBadRequest)
	}

	// Create the account object
	account, err := common.NewAccount(req.Username, req.Firstname, req.Lastname, req.Password, false)
	if err != nil {
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	// Create the account in the database
	id, err := s.accountStore.CreateAccount(account)
	if err != nil {
		if strings.Contains(err.Error(), "value too long") {
			return WriteJSON(w, http.StatusBadRequest, errBadRequest)
		}

		if strings.Contains(err.Error(), "duplicate key value") {
			return WriteJSON(w, http.StatusConflict, errAlreadyExists)
		}

		logger.Error("Error in handleCreateAccount at operation CreateAccount:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	account.ID = id

	// Generate a JWT token
	token, err := generateJWT(account)
	if err != nil {
		logger.Error("Error in handleCreateAccount at operation generateJWT:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	// Write the response
	resp := CreateAccountResponse{
		Token:   token,
		Account: account,
	}

	return WriteJSON(w, http.StatusOK, resp)
}

// Get an account by username
func (s *APIServer) handleGetAccountByUsername(w http.ResponseWriter, r *http.Request) error {
	logger.Debug("Handling get account by username")
	// Retrieve the requesting user
	user, err := s.getAccountByToken(w, r)
	if err != nil {
		return WriteJSON(w, http.StatusUnauthorized, errUnauthorized)
	}

	// Get the requested user
	requestedUser := mux.Vars(r)["username"]

	// Check if the request is for the user itself or if the user is an admin
	if user.Username != requestedUser && !user.Admin {
		return WriteJSON(w, http.StatusForbidden, errForbidden)
	}

	// Get the requested account from the database
	account, err := s.accountStore.GetAccountByUsername(requestedUser)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleGetAccountByUsername at operation GetAccountByUsername:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	return WriteJSON(w, http.StatusOK, account)
}

// Update an account
func (s *APIServer) handleUpdateAccount(w http.ResponseWriter, r *http.Request) error {
	logger.Debug("Handling update account")
	// Retrieve the requesting user
	user, err := s.getAccountByToken(w, r)
	if err != nil {
		return WriteJSON(w, http.StatusUnauthorized, errUnauthorized)
	}

	// Get the requested user
	requestedUser := mux.Vars(r)["username"]

	// Check if the request is for the user itself or if the user is an admin
	if user.Username != requestedUser && !user.Admin {
		return WriteJSON(w, http.StatusForbidden, errForbidden)
	}

	// Decode the request body into an UpdateAccountRequest struct
	req := new(UpdateAccountRequest)
	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		return WriteJSON(w, http.StatusBadRequest, errBadRequest)
	}

	// Get the requested account from the database
	account, err := s.accountStore.GetAccountByUsername(requestedUser)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleUpdateAccount at operation GetAccountByUsername:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	// Update the password if it is requested
	if req.Password != "" {
		encpw, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
		if err != nil {
			logger.Error("Error in handleUpdateAccount at operation GenerateFromPassword:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}

		account.EncryptedPassword = string(encpw)
	}

	// Update the firstname
	account.Firstname = req.Firstname

	// Update the lastname
	account.Lastname = req.Lastname

	// Update the admin status if it is requested
	if req.Admin != account.Admin {
		// Check if the user is an admin
		if !user.Admin {
			return WriteJSON(w, http.StatusForbidden, errForbidden)
		}

		// Prevent editing the admin status of the default admin account
		if account.Username == "admin" {
			return WriteJSON(w, http.StatusTeapot, APIError{Error: "admin rights cannot be removed from admin account you fool"})
		} else {
			account.Admin = req.Admin
		}
	}

	// Update the account in the database
	if err := s.accountStore.UpdateAccount(account); err != nil {
		if strings.Contains(err.Error(), "value too long") {
			return WriteJSON(w, http.StatusBadRequest, errBadRequest)
		}

		logger.Error("Error in handleUpdateAccount at operation UpdateAccount:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	return WriteJSON(w, http.StatusOK, account)
}

// Delete an account
func (s *APIServer) handleDeleteAccount(w http.ResponseWriter, r *http.Request) error {
	logger.Debug("Handling delete account")
	// Retrieve the requesting user
	user, err := s.getAccountByToken(w, r)
	if err != nil {
		return WriteJSON(w, http.StatusUnauthorized, errUnauthorized)
	}

	// Get the requested user
	requestedUser := mux.Vars(r)["username"]

	// Check if the request is for the user itself or if the user is an admin
	if user.Username != requestedUser && !user.Admin {
		return WriteJSON(w, http.StatusForbidden, errForbidden)
	}

	// Get the requested account from the database
	acc, err := s.accountStore.GetAccountByUsername(requestedUser)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleDeleteGame at operation DeleteGame:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	// Prevent deleting the default admin account
	if acc.Username == "admin" {
		user, err := s.getAccountByToken(w, r)
		returnStr := "admin account cannot be deleted"
		if err == nil {
			returnStr = "account " + user.Username + " is deleted"
		}

		return WriteJSON(w, http.StatusUnavailableForLegalReasons, APIError{Error: returnStr})
	}

	// Delete the account from the database
	if err := s.accountStore.DeleteAccount(requestedUser); err != nil {
		logger.Error("Error in handleDeleteAccount at operation DeleteAccount:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	w.WriteHeader(http.StatusNoContent)
	return nil
}

// Generate a JWT token
func generateJWT(account *common.Account) (string, error) {
	claims := &jwt.MapClaims{
		"ExpiresAt":       15000,
		"AccountUsername": account.Username,
		"AccountPassword": account.EncryptedPassword,
	}
	secret := os.Getenv("JWT_SECRET")
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(secret))
}
