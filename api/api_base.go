package api

import (
	"ScoutingGameSiteV2_Backend/common"
	"ScoutingGameSiteV2_Backend/logger"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	jwt "github.com/golang-jwt/jwt/v4"
	"github.com/gorilla/mux"
)

// Start the API server
func (s *APIServer) Run() error {
	router := mux.NewRouter()

	// Handle websocket routes
	router.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		s.handleWebsocket(w, r)
	})

	// Handle account routes
	router.HandleFunc("/api/login", makeHTTPHandlerFunc(s.handleLogin)).Methods(http.MethodPost)
	router.HandleFunc("/api/accounts", makeHTTPHandlerFunc(s.handleGetAccounts)).Methods(http.MethodGet)
	router.HandleFunc("/api/accounts", makeHTTPHandlerFunc(s.handleCreateAccount)).Methods(http.MethodPost)
	router.HandleFunc("/api/accounts/{username}", makeHTTPHandlerFunc(s.handleGetAccountByUsername)).Methods(http.MethodGet)
	router.HandleFunc("/api/accounts/{username}", makeHTTPHandlerFunc(s.handleUpdateAccount)).Methods(http.MethodPut)
	router.HandleFunc("/api/accounts/{username}", makeHTTPHandlerFunc(s.handleDeleteAccount)).Methods(http.MethodDelete)

	// Handle game routes
	router.HandleFunc("/api/games", makeHTTPHandlerFunc(s.handleGames))
	router.HandleFunc("/api/games/{game_id}", makeHTTPHandlerFunc(s.handleGame_id))
	router.HandleFunc("/api/games/{game_id}/reset_users", makeHTTPHandlerFunc(s.handleGameResetUsers))

	// Handle game point routes
	router.HandleFunc("/api/games/{game_id}/locations", makeHTTPHandlerFunc(s.handleGameLocations))
	router.HandleFunc("/api/games/{game_id}/locations/{point_id}", makeHTTPHandlerFunc(s.handleGameLocation_id))

	logger.Infof("JSON API server running on %s", s.listenAddr)

	http.ListenAndServe(s.listenAddr, router)

	return nil
}

// Create a http.HandlerFunc from an apifunc
func makeHTTPHandlerFunc(f apifunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := f(w, r); err != nil {
			WriteJSON(w, http.StatusBadRequest, APIError{Error: err.Error()})
		}
	}
}

// Write a JSON response
func WriteJSON(w http.ResponseWriter, status int, v any) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(v)
}

// Get the account from a JWT token
func (s *APIServer) getAccountByToken(w http.ResponseWriter, r *http.Request) (*common.Account, error) {
	tokenString := r.Header.Get("x-jwt-token")

	// Validate the JWT token
	token, err := validateJWT(tokenString)
	if err != nil {
		return nil, err
	}

	// Check if the token is valid
	if !token.Valid {
		return nil, fmt.Errorf("invalid token")
	}

	// Get the account from the database
	claims := token.Claims.(jwt.MapClaims)
	account, err := s.accountStore.GetAccountByUsername(claims["AccountUsername"].(string))
	if err != nil {
		return nil, fmt.Errorf("invalid token")
	}

	return account, nil
}

// Validate a JWT token
func validateJWT(tokenString string) (*jwt.Token, error) {
	secret := os.Getenv("JWT_SECRET")

	return jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containinf yout secret, e.g. []byte("my_secret_key")
		return []byte(secret), nil
	})
}
