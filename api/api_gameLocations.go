package api

import (
	"ScoutingGameSiteV2_Backend/common"
	"ScoutingGameSiteV2_Backend/logger"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

// Handle a requst to /games/{game_id}/points
func (s *APIServer) handleGameLocations(w http.ResponseWriter, r *http.Request) error {
	// Get the requested game ID
	game_id, err := strconv.Atoi(mux.Vars(r)["game_id"])
	if err != nil {
		return WriteJSON(w, http.StatusBadRequest, errInvalidResource)
	}

	// Get the game
	game, err := s.gameStore.GetGameByID(game_id)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleGameLocations at operation GetGameByID:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	// GET: Get all the points
	// Everybody can do this
	if r.Method == http.MethodGet {
		// handle get all games
		return s.handleGetGameLocations(w, r, game)
	}

	// POST: Create a point
	if r.Method == http.MethodPost {
		// Check if the user is logged in
		user, err := s.getAccountByToken(w, r)
		if err != nil {
			return WriteJSON(w, http.StatusUnauthorized, errUnauthorized)
		}

		if game.OwnerName != user.Username {
			return WriteJSON(w, http.StatusForbidden, errForbidden)
		}

		// Handle create a point
		return s.handleCreatGameLocation(w, r, game, user)
	}

	return fmt.Errorf("method not allowed %s", r.Method)
}

// Handle a requst to /games/{game_id}/points/{point_id}
func (s *APIServer) handleGameLocation_id(w http.ResponseWriter, r *http.Request) error {
	// Get the requested game ID
	game_id, err := strconv.Atoi(mux.Vars(r)["game_id"])
	if err != nil {
		return WriteJSON(w, http.StatusBadRequest, errInvalidResource)
	}

	// Get the requested point ID
	point_id, err := strconv.Atoi(mux.Vars(r)["point_id"])
	if err != nil {
		return WriteJSON(w, http.StatusBadRequest, errInvalidResource)
	}

	// Get the game
	game, err := s.gameStore.GetGameByID(game_id)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleGameLocation_id at operation GetGameByID:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	// Check if the user is logged in
	user, err := s.getAccountByToken(w, r)
	if err != nil {
		return WriteJSON(w, http.StatusUnauthorized, errUnauthorized)
	}

	point, err := s.GameLocationStore.GetGameLocationByID(point_id)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleGameLocation_id at operation GetGameLocationByID:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	if game.OwnerName != user.Username || point.Game_id != game.ID {
		return WriteJSON(w, http.StatusForbidden, errForbidden)
	}

	// GET: Get a point by id (whitin the game)
	if r.Method == http.MethodGet {
		// handle get point by id
		return s.handleGetGameLocationByID(w, r, game, user, point)
	}

	// PUT: Update a point
	if r.Method == http.MethodPut {
		return s.handleUpdateGameLocation(w, r, game, user, point)
	}

	// DELETE: Delete a point
	if r.Method == http.MethodDelete {
		return s.handleDeleteGameLocation(w, r, game, user, point)
	}

	return fmt.Errorf("method not allowed %s", r.Method)
}

// Get all the points
func (s *APIServer) handleGetGameLocations(w http.ResponseWriter, r *http.Request, game *common.Game) error {
	logger.Debug("Handling get game all locations by ID")

	points, err := s.GameLocationStore.GetGameLocationsByGameID(game.ID)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleGetGameLocations at operation GetGameLocationsByGameID:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	return WriteJSON(w, http.StatusOK, points)
}

// Create a point
func (s *APIServer) handleCreatGameLocation(w http.ResponseWriter, r *http.Request, game *common.Game, user *common.Account) error {
	logger.Debug("Handling create game location")

	// Decode the request
	req := new(CreateGameLocationRequest)
	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		return WriteJSON(w, http.StatusBadRequest, errBadRequest)
	}

	point := common.NewGameLocation(game.ID, req.Latitude, req.Longitude, req.Radius, req.Color, req.Settings)
	point_id, err := s.GameLocationStore.CreateGameLocation(point)
	if err != nil {
		if strings.Contains(err.Error(), "value too long") {
			return WriteJSON(w, http.StatusBadRequest, errBadRequest)
		}

		logger.Error("Error in handleCreatGameLocation at operation CreateGameLocation:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	point.ID = point_id

	if err := s.gameStore.UpdateGameEditDate(game.ID); err != nil {
		logger.Error("Error in handleUpdateGameLocation at operation UpdateGameEditDate:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	if err := s.WebsocketSendLocation(game.ID, point, "new_location"); err != nil {
		logger.Error("Error in handleUpdateGameLocation at operation WebsocketSendNewLocation:")
		logger.Error(err)
	}

	return WriteJSON(w, http.StatusOK, point)
}

// Get a point by id
func (s *APIServer) handleGetGameLocationByID(w http.ResponseWriter, r *http.Request, game *common.Game, user *common.Account, point *common.GameLocations) error {
	logger.Debug("Handling get game location by ID")
	return WriteJSON(w, http.StatusOK, point)
}

// Update a point
func (s *APIServer) handleUpdateGameLocation(w http.ResponseWriter, r *http.Request, game *common.Game, user *common.Account, point *common.GameLocations) error {
	logger.Debug("Handling update a game location")

	// Decode the request
	req := new(UpdateGameLocationRequest)
	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		return WriteJSON(w, http.StatusBadRequest, errInvalidResource)
	}

	// Apply the request to the point struct
	if req.Latitude != 0 {
		point.Latitude = req.Latitude
	}

	if req.Longitude != 0 {
		point.Longitude = req.Longitude
	}

	if req.Radius != 0 {
		point.Radius = req.Radius
	}

	// if req.Owner != "" {
	point.Owner = req.Owner
	// }

	if req.Color != "" {
		point.Color = req.Color
	}

	if req.Settings != nil {
		point.Settings = req.Settings
	}

	// Update the point
	if err := s.GameLocationStore.UpdateGameLocation(point); err != nil {
		if strings.Contains(err.Error(), "value too long") {
			return WriteJSON(w, http.StatusBadRequest, errBadRequest)
		}

		logger.Error("Error in handleUpdateGameLocation at operation UpdateGameLocation:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	if err := s.gameStore.UpdateGameEditDate(game.ID); err != nil {
		logger.Error("Error in handleUpdateGameLocation at operation UpdateGameEditDate:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	if err := s.WebsocketSendLocation(game.ID, point, "update_location"); err != nil {
		logger.Error("Error in handleUpdateGameLocation at operation WebsocketSendNewLocation:")
		logger.Error(err)
	}

	return WriteJSON(w, http.StatusOK, point)
}

// Delete a point
func (s *APIServer) handleDeleteGameLocation(w http.ResponseWriter, r *http.Request, game *common.Game, user *common.Account, point *common.GameLocations) error {
	logger.Debug("Handling delete game location")

	if err := s.GameLocationStore.DeleteGameLocation(point.ID); err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleDeleteGame at operation DeleteGame:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	if err := s.gameStore.UpdateGameEditDate(game.ID); err != nil {
		logger.Error("Error in handleUpdateGameLocation at operation UpdateGameEditDate:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	if err := s.WebsocketSendLocation(game.ID, point, "remove_location"); err != nil {
		logger.Error("Error in handleUpdateGameLocation at operation WebsocketSendNewLocation:")
		logger.Error(err)
	}

	return WriteJSON(w, http.StatusOK, APIMessage{Message: "point deleted"})
}
