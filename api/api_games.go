package api

import (
	"ScoutingGameSiteV2_Backend/common"
	"ScoutingGameSiteV2_Backend/logger"
	"ScoutingGameSiteV2_Backend/storage"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

// Handle a request to /games
func (s *APIServer) handleGames(w http.ResponseWriter, r *http.Request) error {
	// Get the user
	user, err := s.getAccountByToken(w, r)
	if err != nil {
		return WriteJSON(w, http.StatusUnauthorized, errUnauthorized)
	}

	// GET: Get all games
	if r.Method == http.MethodGet {
		// handel get all games
		return s.handleGetGames(w, r, user)
	}

	// POST: Create a game
	if r.Method == http.MethodPost {
		return s.handleCreateGame(w, r, user)
	}

	return fmt.Errorf("method not allowed %s", r.Method)
}

// Handle a request to /games/{game_id}
func (s *APIServer) handleGame_id(w http.ResponseWriter, r *http.Request) error {
	// get the game id
	game_id, err := strconv.Atoi(mux.Vars(r)["game_id"])
	if err != nil {
		return WriteJSON(w, http.StatusBadRequest, errInvalidResource)
	}

	// Get the game
	game, err := s.gameStore.GetGameByID(game_id)
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleGame_id at operation GetGameByID:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	// GET: Get a game by id
	if r.Method == http.MethodGet {
		return s.handleGetGameByID(w, r, game_id, game)
	} else {
		// Get the user
		user, err := s.getAccountByToken(w, r)
		if err != nil {
			return WriteJSON(w, http.StatusUnauthorized, errUnauthorized)
		}

		// PUT: Update a game
		if r.Method == http.MethodPut {
			return s.handleUpdateGame(w, r, game_id, game, user)
		}

		// DELETE: Delete a game
		if r.Method == http.MethodDelete {
			return s.handleDeleteGame(w, r, game_id, game, user)
		}
	}

	return fmt.Errorf("method not allowed %s", r.Method)
}

// Handle a request to /games/{game_id}/reset_users
func (s *APIServer) handleGameResetUsers(w http.ResponseWriter, r *http.Request) error {
	// get the game id
	game_id, err := strconv.Atoi(mux.Vars(r)["game_id"])
	if err != nil {
		return WriteJSON(w, http.StatusBadRequest, errInvalidResource)
	}

	// GET: Get a game by id
	if r.Method == http.MethodGet {
		s.resetAllActiveUsers(game_id)
		return WriteJSON(w, http.StatusOK, APIMessage{Message: "reset active users done"})
	}
	return fmt.Errorf("method not allowed %s", r.Method)
}

// Get a game by id
func (s *APIServer) handleGetGameByID(w http.ResponseWriter, r *http.Request, gameID int, game *common.Game) error {
	logger.Debug("Handling get game by ID")
	// Return the game
	return WriteJSON(w, http.StatusOK, game)
}

// Get all games of user
func (s *APIServer) handleGetGames(w http.ResponseWriter, r *http.Request, user *common.Account) error {
	logger.Debug("Handling get all games of user")

	// Get all games
	games, err := s.gameStore.GetGamesByOwnerName(user.Username)
	if err != nil {
		logger.Error("Error in handleGetGames at operation GetGamesByOwnerName:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	// Return the games
	return WriteJSON(w, http.StatusOK, games)
}

// Create a game
func (s *APIServer) handleCreateGame(w http.ResponseWriter, r *http.Request, user *common.Account) error {
	logger.Debug("Handling create a game")

	// Decode the request
	req := new(CreateGameRequest)
	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		return WriteJSON(w, http.StatusBadRequest, errBadRequest)
	}

	// Create the game
	game := common.NewGame(req.Name, common.GameType(req.GameType), user.Username)
	game_id, err := s.gameStore.CreateGame(game)
	if err != nil {
		logger.Error("Error in handleCreateGame at operation CreateGame:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	game.ID = game_id

	// Return the game
	return WriteJSON(w, http.StatusOK, game)
}

// Update a game
func (s *APIServer) handleUpdateGame(w http.ResponseWriter, r *http.Request, game_id int, game *common.Game, user *common.Account) error {
	logger.Debug("Handling update a game")

	// Check if the game is owned by the user or if the user is an admin
	if game.OwnerName != user.Username || !user.Admin {
		return WriteJSON(w, http.StatusForbidden, errForbidden)
	}

	// Decode the request
	req := new(UpdateGameRequest)
	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		return WriteJSON(w, http.StatusBadRequest, errBadRequest)
	}

	// Update the game
	if req.Name != "" {
		game.Name = req.Name
	}

	game.Settings = req.Settings

	game.DateEdit = time.Now().UTC()

	if err := s.gameStore.UpdateGame(game); err != nil {
		logger.Error("Error in handleUpdateGame at operation UpdateGame:")
		logger.Error(err)
		return WriteJSON(w, http.StatusInternalServerError, errInternal)
	}

	if err := s.WebsocketSendGameUpdate(game); err != nil {
		logger.Error(err)
	}

	// Return the game
	return WriteJSON(w, http.StatusOK, game)
}

// Delete a game
func (s *APIServer) handleDeleteGame(w http.ResponseWriter, r *http.Request, game_id int, game *common.Game, user *common.Account) error {
	logger.Debug("Handling delete a game")

	// Check if the game is owned by the user or if the user is an admin
	if game.OwnerName != user.Username || !user.Admin {
		return WriteJSON(w, http.StatusForbidden, errForbidden)
	}

	// Delete the game
	if err := s.gameStore.DeleteGame(game_id); err != nil {
		if strings.Contains(err.Error(), "not found") {
			return WriteJSON(w, http.StatusNotFound, errNotFound)
		} else {
			logger.Error("Error in handleDeleteGame at operation DeleteGame:")
			logger.Error(err)
			return WriteJSON(w, http.StatusInternalServerError, errInternal)
		}
	}

	// Return the game
	return WriteJSON(w, http.StatusOK, APIMessage{Message: "game deleted"})
}

func (s *APIServer) resetAllActiveUsers(game_id int) {
	if _, ok := storage.ActiveUsers[game_id]; !ok {
		storage.ActiveUsers[game_id] = make([]storage.ActiveUser, 0)
	}
}
