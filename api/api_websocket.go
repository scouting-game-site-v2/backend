package api

import (
	"ScoutingGameSiteV2_Backend/common"
	"ScoutingGameSiteV2_Backend/logger"
	"ScoutingGameSiteV2_Backend/storage"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

// Handel websocket requests
func (s *APIServer) handleWebsocket(w http.ResponseWriter, r *http.Request) error {
	logger.Debug("Handling websocket request")
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	conn, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		logger.Error("Error in handleWebsocket at operation Upgrade:")
		logger.Error(err)
		return err
	}

	defer conn.Close()

	logger.Debug("Client Connected")

	if err := reader(conn, s); err != nil {
		logger.Error("Error in handleWebsocket at operation reader:")
		logger.Error(err)
		return err
	}

	return nil
}

func reader(conn *websocket.Conn, s *APIServer) error {
	for {
		// Check if connection is still open
		if conn == nil || !_isConnectionOpen(conn) {
			logger.Debug("Connection closed, stopping reader")
			return nil
		}

		_, messageData, err := conn.ReadMessage()
		if err != nil {
			var gameID = -1
			for userGameID, activeUser := range storage.ActiveUsers {
				for _, user := range activeUser {
					if user.Conn == conn {
						gameID = userGameID
						break
					}
				}
			}

			logger.Errorf("Error in reader at operation ReadMessage: (With gameID: %d)", gameID)
			return err
		}

		var msgData WebSocketMessage
		err = json.Unmarshal(messageData, &msgData)
		if err != nil {
			logger.Error("Error in reader at operation Unmarshal:")
			return err
		} else {
			data, err := json.Marshal(msgData.Data)
			if err != nil {
				return err
			} else {
				switch msgData.Action {
				case "join_game":
					err = joinGame(data, conn, s)
				case "leave_game":
					err = leaveGame(data, conn, s)
				case "update_location":
					err = updateLocation(data, conn, s)
				case "get_active_colors":
					err = getActiveColors(data, conn, s)
				case "set_active_color":
					err = setActiveColor(data, conn, s)
				case "get_active_user_count":
					err = getActiveUserCount(conn, s)
				case "close_conn":
					_safeClose(conn)
				case "test":
					err = _test(data, conn, s)
				default:
					logger.Errorf("invalid action: %s", msgData.Action)
				}

				if err != nil {
					logger.Error(err)
				}
			}
		}
	}
}

func _safeClose(conn *websocket.Conn) {
	err := conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	if err != nil {
		logger.Error("Error in safeClose at operation WriteMessage:")
		logger.Error(err)
	}
	conn.Close()
}

// Join game
func joinGame(messageData []byte, conn *websocket.Conn, s *APIServer) error {
	logger.Debug("Handling join game")

	// Decode message
	var gameData WebSocketJoinGameData
	err := json.Unmarshal(messageData, &gameData)
	if err != nil {
		logger.Error("Error in joinGame at operation Unmarshal:")
		return err
	}

	// Check if user is already in the list
	activeUsers, ok := storage.ActiveUsers[gameData.GameID]
	if ok {
		for _, user := range activeUsers {
			if user.Conn == conn {
				// User is already in the list
				logger.Debug("User already joined the game")
				return nil
			}
		}
	}

	userData := storage.ActiveUser{
		GameUsername: gameData.GameUsername,
		Color:        gameData.Color,
		Conn:         conn,
	}

	// Add user to active users
	if _, ok := storage.ActiveUsers[gameData.GameID]; !ok {
		storage.ActiveUsers[gameData.GameID] = make([]storage.ActiveUser, 0)
	}
	storage.ActiveUsers[gameData.GameID] = append(storage.ActiveUsers[gameData.GameID], userData)

	msg := WebSocketMessage{
		Action: "join_responce",
		Data: WebSocketJoinGameData{
			GameID:       gameData.GameID,
			GameUsername: userData.GameUsername,
			Color:        userData.Color,
		},
	}

	// send join message to user
	msgBytes, err := json.Marshal(msg)
	if err != nil {
		logger.Error("Error in joinGame at operation Marshal:")
		return err
	}

	if err := userData.Conn.WriteMessage(websocket.TextMessage, msgBytes); err != nil {
		logger.Error("Error in joinGame at operation WriteMessage:")
		return err
	}

	if err := sendUserCount(gameData.GameID); err != nil {
		logger.Error("Error in joinGame at operation sendUserCount:")
		return err
	}

	return nil
}

// leave game
func leaveGame(messageData []byte, conn *websocket.Conn, s *APIServer) error {
	logger.Debug("Handling leave game")

	// Decode message
	var gameData WebSocketGetLeaveData
	err := json.Unmarshal(messageData, &gameData)
	if err != nil {
		logger.Error("Error in leaveGame at operation Unmarshal:")
		return err
	}

	// Remove user from active users
	_removeUser(gameData.GameID, conn)

	// send the new user count to all the game users
	if err := sendUserCount(gameData.GameID); err != nil {
		logger.Error("Error in leaveGame at operation sendUserCount:")
		return err
	}

	// send the new active color list to all the game users
	colors := _getAllActiveColors(gameData.GameID)

	msg := WebSocketMessage{
		Action: "active_colors",
		Data:   colors,
	}

	if errs := _sendToAllUsersOfGame(msg, gameData.GameID); len(errs) > 0 {
		return fmt.Errorf("errors: %v", errs)
	}

	// Close websocket connection if requested
	if gameData.CloseConn {
		_safeClose(conn)
	}

	return nil
}

// send a location update to all users
func updateLocation(messageData []byte, conn *websocket.Conn, s *APIServer) error {
	logger.Debug("Handling updateLocation")

	// Decode message
	var gameData WebSocketUpdateLocationData
	err := json.Unmarshal(messageData, &gameData)
	if err != nil {
		logger.Error("Error in updateLocation at operation Unmarshal:")
		return err
	}

	var userData storage.ActiveUser
	for _, user := range storage.ActiveUsers[gameData.GameID] {
		if user.Conn == conn {
			userData = user
			break
		}
	}

	// check if a user is found
	if userData == (storage.ActiveUser{}) {
		return fmt.Errorf("user not found")
	}

	// get the location from storage
	location, err := s.GameLocationStore.GetGameLocationByID(gameData.LocationID)
	if err != nil {
		logger.Error("Error in updateLocation at operation GetGameLocationByID:")
		logger.Error(err)
	}

	// update location with the data of the user
	location.Color = userData.Color
	location.Owner = userData.GameUsername

	// write the new location data to storage
	if err := s.GameLocationStore.UpdateGameLocation(location); err != nil {
		logger.Error("Error in updateLocation at operation UpdateGameLocation:")
		logger.Error(err)
	}

	s.WebsocketSendLocation(gameData.GameID, location, "update_location")

	return nil
}

func sendUserCount(game_id int) error {
	msg := WebSocketMessage{
		Action: "user_count",
		Data:   len(storage.ActiveUsers[game_id]),
	}

	if errs := _sendToAllUsersOfGame(msg, game_id); len(errs) > 0 {
		return fmt.Errorf("errors: %v", errs)
	}

	return nil
}

// send test to all clients of a game
func _test(messageData []byte, conn *websocket.Conn, s *APIServer) error {
	logger.Debug("Handling test")

	// Decode message
	var gameData WebSocketGetGameData
	err := json.Unmarshal(messageData, &gameData)
	if err != nil {
		logger.Error("Error in _test at operation Unmarshal:")
		return err
	}

	errs := make([]error, 0)

	// Get active users for game
	if activeUsers, ok := storage.ActiveUsers[gameData.GameID]; ok {
		// Send test message to all active users
		for _, userData := range activeUsers {
			// Check if connection is open
			if !_isConnectionOpen(userData.Conn) {
				// Remove user from active users
				_removeUser(gameData.GameID, conn)
				continue
			}

			message := fmt.Sprintf("Test message from the server with %d user(s) in game %d", len(storage.ActiveUsers[gameData.GameID]), gameData.GameID)
			if err := userData.Conn.WriteMessage(websocket.TextMessage, []byte(message)); err != nil {
				logger.Error(err)
				errs = append(errs, err)
			}
		}
	}

	logger.Debugf("Amount of active users in game: %d", len(storage.ActiveUsers[gameData.GameID]))

	if len(errs) > 0 {
		logger.Error("Error in _test at operation WriteMessage:")
		return fmt.Errorf("errors: %v", errs)
	}

	return nil
}

func getActiveColors(messageData []byte, conn *websocket.Conn, s *APIServer) error {
	logger.Debug("Handling getActiveColors")

	// Decode message
	var gameData WebSocketGetGameData
	err := json.Unmarshal(messageData, &gameData)
	if err != nil {
		logger.Error("Error in getActiveColors at operation Unmarshal:")
		return err
	}

	colors := _getAllActiveColors(gameData.GameID)

	msg := WebSocketMessage{
		Action: "active_colors",
		Data:   colors,
	}

	msgBytes, err := json.Marshal(msg)
	if err != nil {
		logger.Error("Error in getActiveColors at operation Marshal:")
		return err
	}

	if err := conn.WriteMessage(websocket.TextMessage, msgBytes); err != nil {
		logger.Error("Error in getActiveColors at operation WriteMessage:")
		return err
	}

	return nil
}

func setActiveColor(messageData []byte, conn *websocket.Conn, s *APIServer) error {
	logger.Debug("Handling getActiveColors")

	// Decode message
	var gameData WebSocketSetUserColor
	err := json.Unmarshal(messageData, &gameData)
	if err != nil {
		logger.Error("Error in setActiveColor at operation Unmarshal:")
		return err
	}

	for i, user := range storage.ActiveUsers[gameData.GameID] {
		if user.Conn == conn {
			storage.ActiveUsers[gameData.GameID][i].Color = gameData.Color
		}
	}

	colors := _getAllActiveColors(gameData.GameID)

	msg := WebSocketMessage{
		Action: "active_colors",
		Data:   colors,
	}

	if errs := _sendToAllUsersOfGame(msg, gameData.GameID); len(errs) > 0 {
		return fmt.Errorf("errors: %v", errs)
	}

	return nil
}

func getActiveUserCount(conn *websocket.Conn, s *APIServer) error {
	logger.Debug("Handeling getActiveUserCount")

	activeUserCount := 0
	for _, activeGame := range storage.ActiveUsers {
		activeUserCount += len(activeGame)
	}

	msg := WebSocketMessage{
		Action: "active_user_count",
		Data:   activeUserCount,
	}

	msgBytes, err := json.Marshal(msg)
	if err != nil {
		logger.Error("Error in getActiveColors at operation Marshal:")
		return err
	}

	if err := conn.WriteMessage(websocket.TextMessage, msgBytes); err != nil {
		logger.Error("Error in getActiveColors at operation WriteMessage:")
		return err
	}

	return nil
}

func _getAllActiveColors(gameID int) []string {
	colors := make([]string, 0)

	// Get active users for game
	if activeUsers, ok := storage.ActiveUsers[gameID]; ok {
		// Send test message to all active users
		for _, userData := range activeUsers {
			// Check if connection is open
			if !_isConnectionOpen(userData.Conn) {
				// Remove user from active users
				_removeUser(gameID, userData.Conn)
				continue
			}

			colors = append(colors, userData.Color)
		}
	}

	return colors
}

func _removeUser(gameID int, conn *websocket.Conn) {
	// Remove user from active users
	for i, user := range storage.ActiveUsers[gameID] {
		if user.Conn == conn {
			storage.ActiveUsers[gameID] = append(storage.ActiveUsers[gameID][:i], storage.ActiveUsers[gameID][i+1:]...)
			break
		}
	}
}

func _isConnectionOpen(conn *websocket.Conn) bool {
	err := conn.WriteMessage(websocket.PingMessage, []byte{})
	return err == nil
}

func _sendToAllUsersOfGame(message any, game_id int) []error {
	errs := make([]error, 0)

	// Marshal the message
	msgBytes, err := json.Marshal(message)
	if err != nil {
		errs = append(errs, err)
		logger.Error("Error in _sendToAllUsersOfGame at operation Marshal:")
		return errs
	}

	// Get active users for game
	if activeUsers, ok := storage.ActiveUsers[game_id]; ok {
		// Send test message to all active users
		for _, userData := range activeUsers {
			// Check if connection is open
			if !_isConnectionOpen(userData.Conn) {
				// Remove user from active users
				_removeUser(game_id, userData.Conn)
				continue
			}

			if err := userData.Conn.WriteMessage(websocket.TextMessage, msgBytes); err != nil {
				logger.Error("Error in _sendToAllUsersOfGame at operation WriteMessage:")
				errs = append(errs, err)
			}
		}
	}

	return errs
}

func (s *APIServer) WebsocketSendLocation(game_id int, location *common.GameLocations, action string) error {
	logger.Debug("Handling websocket send location, action: " + action)

	msg := WebSocketMessage{
		Action: action,
		Data:   location,
	}

	if errs := _sendToAllUsersOfGame(msg, game_id); len(errs) > 0 {
		return fmt.Errorf("errors: %v", errs)
	}

	return nil
}

func (s *APIServer) WebsocketSendGameUpdate(game *common.Game) error {
	logger.Debug("Handling websocket send game update")

	msg := WebSocketMessage{
		Action: "game_update",
		Data:   game,
	}

	if errs := _sendToAllUsersOfGame(msg, game.ID); len(errs) > 0 {
		return fmt.Errorf("errors: %v", errs)
	}

	return nil
}
