package api

import (
	"ScoutingGameSiteV2_Backend/common"
	"ScoutingGameSiteV2_Backend/storage"
	"net/http"
)

type APIServer struct {
	listenAddr        string
	accountStore      storage.AccountStorage
	gameStore         storage.GameStorage
	GameLocationStore storage.GameLocationStorage
}

type apifunc func(w http.ResponseWriter, r *http.Request) error

type APIError struct {
	Error string `json:"error"`
}

var (
	errUnauthorized    = APIError{Error: "unauthorized, please login"}
	errInvalidPassword = APIError{Error: "invalid password"}
	errForbidden       = APIError{Error: "forbidden"}
	errInvalidResource = APIError{Error: "invalid resource id"}
	errNotFound        = APIError{Error: "not found"}
	errBadRequest      = APIError{Error: "bad request data"}
	errAlreadyExists   = APIError{Error: "resource already exists"}
	errInternal        = APIError{Error: "internal server error"}
)

type APIMessage struct {
	Message string `json:"message"`
}

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Account *common.Account `json:"account"`
	Token   string          `json:"token"`
}

type CreateAccountRequest struct {
	Username  string `json:"username"`
	Password  string `json:"password"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

type CreateAccountResponse struct {
	Account *common.Account `json:"account"`
	Token   string          `json:"token"`
}

type UpdateAccountRequest struct {
	Password  string `json:"password"`
	Admin     bool   `json:"admin"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

type CreateGameRequest struct {
	Name     string          `json:"name"`
	GameType common.GameType `json:"game_type"`
}

type UpdateGameRequest struct {
	Name     string              `json:"name"`
	Settings common.GameSettings `json:"settings"`
}

type CreateGameLocationRequest struct {
	Latitude  float32        `json:"latitude"`
	Longitude float32        `json:"longitude"`
	Radius    int            `json:"radius"`
	Color     string         `json:"color"`
	Settings  map[string]any `json:"settings,omitempty"`
}

type UpdateGameLocationRequest struct {
	Latitude  float32        `json:"latitude"`
	Longitude float32        `json:"longitude"`
	Radius    int            `json:"radius"`
	Owner     string         `json:"owner"`
	Color     string         `json:"color"`
	Settings  map[string]any `json:"settings,omitempty"`
}

type WebSocketMessage struct {
	Action string `json:"action"`
	Data   any    `json:"data"`
}

type WebSocketGetGameData struct {
	GameID int `json:"game_id"`
}

type WebSocketGetLeaveData struct {
	GameID    int  `json:"game_id"`
	CloseConn bool `json:"close_conn"`
}

type WebSocketJoinGameData struct {
	GameID       int    `json:"game_id"`
	GameUsername string `json:"game_username"`
	Color        string `json:"color"`
}

type WebSocketUpdateLocationData struct {
	GameID     int `json:"game_id"`
	LocationID int `json:"location_id"`
}

type WebSocketSetUserColor struct {
	GameID int    `json:"game_id"`
	Color  string `json:"color"`
}

func NewAPIServer(listenAddr string, accountStore storage.AccountStorage, gameStore storage.GameStorage, GameLocationStore storage.GameLocationStorage) *APIServer {
	return &APIServer{
		listenAddr:        listenAddr,
		accountStore:      accountStore,
		gameStore:         gameStore,
		GameLocationStore: GameLocationStore,
	}
}
