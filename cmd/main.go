// Description: main file for the api server
// On first run, you need to seed the database with the -seed flag

// go mod init ScoutingGameSiteV2/backend
package main

import (
	"ScoutingGameSiteV2_Backend/api"
	"ScoutingGameSiteV2_Backend/common"
	"ScoutingGameSiteV2_Backend/logger"
	"ScoutingGameSiteV2_Backend/storage"
	"os"
	"strings"
)

// Seed the database with the default admin account
func seedAccount(store *storage.PostgresStore, username, firstname, lastname, password string, admin bool) *common.Account {
	// Create the account
	acc, err := common.NewAccount(username, firstname, lastname, password, admin)
	if err != nil {
		logger.Error(err)
		os.Exit(1)
	}

	// Store the account in the database
	if _, err := store.CreateAccount(acc); err != nil {
		logger.Error(err)
		os.Exit(1)
	}

	logger.Infof("Created account: %s", acc.Username)

	return acc
}

// Check if the required environment variables are set
func checkVariables() {
	if os.Getenv("POSTGRES_PASSWORD") == "" {
		logger.Error("POSTGRES_PASSWORD environment variable not set")
		os.Exit(1)
	}

	if os.Getenv("JWT_SECRET") == "" {
		logger.Error("JWT_SECRET environment variable not set")
		os.Exit(1)
	}
}

func main() {
	checkVariables()

	logger.Info("Starting up...")

	// Connect to the database
	store, err := storage.NewPostgresStore()
	if err != nil {
		logger.Error(err)
		os.Exit(1)
	}

	// Initialize the database
	if err := store.Init(); err != nil {
		logger.Error(err)
		os.Exit(1)
	}

	// Look for the default admin account, if it doesn't exist, create it
	_, err = store.GetAccountByUsername("admin")
	if err != nil {
		if strings.Contains(err.Error(), "account not found") {
			seedAccount(store, "admin", "Admin", "", "changeme", true)
		} else {
			logger.Error(err)
			os.Exit(1)
		}

	}

	// Start the API server
	host, found := os.LookupEnv("LISTEN_HOST")
	if !found {
		// Default host for running as a Docker container
		host = "0.0.0.0:8080"
	}
	server := api.NewAPIServer(host, store, store, store)
	server.Run()
}
