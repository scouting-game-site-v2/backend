package common

import (
	"time"

	"golang.org/x/crypto/bcrypt"
)

type Account struct {
	ID                int       `json:"id"`
	Username          string    `json:"username"`
	Firstname         string    `json:"firstname"`
	Lastname          string    `json:"lastname"`
	EncryptedPassword string    `json:"-"`
	Admin             bool      `json:"admin"`
	CreatedAt         time.Time `json:"created_at"`
	OwnedGames        int       `json:"owned_games"`
}

func (a *Account) ValidPassword(password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(a.EncryptedPassword), []byte(password)) == nil
}

func NewAccount(username, firstname, lastname, password string, admin bool) (*Account, error) {
	encpw, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	return &Account{
		Username:          username,
		Firstname:         firstname,
		Lastname:          lastname,
		EncryptedPassword: string(encpw),
		Admin:             admin,
		CreatedAt:         time.Now().UTC(),
		OwnedGames:        0,
	}, nil
}

type Game struct {
	ID          int          `json:"id"`
	Name        string       `json:"name"`
	GameType    GameType     `json:"game_type"`
	ActiveUsers string       `json:"active_users"`
	CreatedAt   time.Time    `json:"created_at"`
	DateEdit    time.Time    `json:"date_edit"`
	Settings    GameSettings `json:"settings"`
	OwnerName   string       `json:"owner_name"`
}

type GameLocations struct {
	ID        int            `json:"id"`
	Game_id   int            `json:"game_id"`
	Latitude  float32        `json:"latitude"`
	Longitude float32        `json:"longitude"`
	Radius    int            `json:"radius"`
	Owner     string         `json:"owner"`
	Color     string         `json:"color"`
	Settings  map[string]any `json:"settings,omitempty"`
}

// game settings
type GameSettings struct {
	Active          bool           `json:"active"`
	StartTime       time.Time      `json:"start_time"`
	EnableStartTime bool           `json:"enable_start_time"`
	StopTime        time.Time      `json:"stop_time"`
	EnableStopTime  bool           `json:"enable_stop_time"`
	Settings        map[string]any `json:"settings,omitempty"`
}

type GameType int

const (
	Game_1 GameType = 0
	Game_2 GameType = 1
	Game_3 GameType = 2
)

// new game
func NewGame(name string, gameType GameType, ownerName string) *Game {
	// apply default game settings
	settings := defaultGameSettings()
	return &Game{
		Name:        name,
		GameType:    gameType,
		ActiveUsers: "",
		CreatedAt:   time.Now().UTC(),
		DateEdit:    time.Now().UTC(),
		Settings:    *settings,
		OwnerName:   ownerName,
	}
}

// default game settings
func defaultGameSettings() *GameSettings {
	return &GameSettings{
		Active:          true,
		EnableStartTime: false,
		EnableStopTime:  false,
		Settings:        map[string]any{},
	}
}

// Create a new game location
func NewGameLocation(game_id int, lat, lon float32, radius int, color string, settings map[string]any) *GameLocations {
	return &GameLocations{
		Game_id:   game_id,
		Latitude:  lat,
		Longitude: lon,
		Radius:    radius,
		Color:     color,
		Settings:  settings,
	}
}
