package common

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewAccount(t *testing.T) {
	acc, err := NewAccount("JohnDoe", "John", "Doe", "pass", false)
	assert.Nil(t, err)

	fmt.Printf("%+v\n", acc)
}