package logger

import (
	"fmt"
	"time"
)

func Debug(msg any) {
	fmt.Print("\u001b[33m")
	base := fmt.Sprintf("| %s | DEBUG | ", time.Now().UTC().Format("02-01-2006 15:04:05"))
	fmt.Print(base)
	fmt.Print(msg)
	fmt.Println("\u001b[0m")
}

func Debugf(msg string, args ...any) {
	fmt.Print("\u001b[33m")
	base := fmt.Sprintf("| %s | DEBUG | ", time.Now().UTC().Format("02-01-2006 15:04:05"))
	fmt.Print(base)
	message := fmt.Sprintf(msg, args...)
	fmt.Print(message)
	fmt.Println("\u001b[0m")
}

func Info(msg any) {
	fmt.Print("\u001b[34m")
	base := fmt.Sprintf("| %s | INFO | ", time.Now().UTC().Format("02-01-2006 15:04:05"))
	fmt.Print(base)
	fmt.Print(msg)
	fmt.Println("\u001b[0m")
}

func Infof(msg string, args ...any) {
	fmt.Print("\u001b[34m")
	base := fmt.Sprintf("| %s | INFO | ", time.Now().UTC().Format("02-01-2006 15:04:05"))
	fmt.Print(base)
	message := fmt.Sprintf(msg, args...)
	fmt.Print(message)
	fmt.Println("\u001b[0m")
}

func Error(msg any) {
	fmt.Print("\u001b[31m")
	base := fmt.Sprintf("| %s | ERROR | ", time.Now().UTC().Format("02-01-2006 15:04:05"))
	fmt.Print(base)
	fmt.Print(msg)
	fmt.Println("\u001b[0m")
}

func Errorf(msg string, args ...any) {
	fmt.Print("\u001b[31m")
	base := fmt.Sprintf("| %s | ERROR | ", time.Now().UTC().Format("02-01-2006 15:04:05"))
	fmt.Print(base)
	message := fmt.Sprintf(msg, args...)
	fmt.Print(message)
	fmt.Println("\u001b[0m")
}
