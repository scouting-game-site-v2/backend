package storage

import (
	"ScoutingGameSiteV2_Backend/common"
	"database/sql"
	"fmt"
)

// Create the account table
func (s *PostgresStore) CreateAccountTable() error {
	query := `CREATE TABLE IF NOT EXISTS accounts (
		id serial PRIMARY KEY,
		username varchar(100) UNIQUE NOT NULL,
		firstname varchar(100),
		lastname varchar(100),
		encrypted_password varchar(100) NOT NULL,
		admin boolean DEFAULT FALSE,
		created_at timestamp NOT NULL,
		owned_games int DEFAULT 0
	)`
	_, err := s.db.Exec(query)
	return err
}

// Create an account
func (s *PostgresStore) CreateAccount(acc *common.Account) (int, error) {
	lastID := 0
	query := `INSERT INTO accounts 
		(username, firstname, lastname, encrypted_password, admin, created_at)
		VALUES 
		($1, $2, $3, $4, $5, $6)
		RETURNING id
		`
	rows, err := s.db.Query(
		query,
		acc.Username,
		acc.Firstname,
		acc.Lastname,
		acc.EncryptedPassword,
		acc.Admin,
		acc.CreatedAt)

	if err != nil {
		return lastID, err
	}

	defer rows.Close()
	rows.Next()
	err = rows.Scan(&lastID)
	if err != nil {
		return lastID, err
	}

	return lastID, nil
}

// Update an account
func (s *PostgresStore) UpdateAccount(acc *common.Account) error {
	// update only the password, admin, firstname, lastname
	query := `UPDATE accounts SET 
			encrypted_password = $1, 
			admin = $2, 
			firstname = $3, 
			lastname = $4 
			WHERE id = $5
		`
	_, err := s.db.Exec(
		query,
		acc.EncryptedPassword,
		acc.Admin,
		acc.Firstname,
		acc.Lastname,
		acc.ID)

	if err != nil {
		return err
	}
	return nil
}

// Delete an account
func (s *PostgresStore) DeleteAccount(username string) error {
	// look for the account
	_, getErr := s.GetAccountByUsername(username)
	if getErr != nil {
		return getErr
	}

	query := `DELETE FROM accounts WHERE username = $1`

	_, err := s.db.Exec(query, username)
	return err
}

// Get an account by username
func (s *PostgresStore) GetAccountByUsername(username string) (*common.Account, error) {
	query := `SELECT * FROM accounts WHERE username = $1`

	// Get account from DB
	rows, err := s.db.Query(query, username)
	if err != nil {
		return nil, err
	}

	// Close the rows when the function returns
	defer rows.Close()

	if !rows.Next() {
		return nil, fmt.Errorf("account not found for %s", username)
	}

	account, err := scanIntoAccount(rows)
	if err != nil {
		return nil, err
	}

	// Check if there are multiple accounts with the same username
	if rows.Next() {
		return nil, fmt.Errorf("multiple accounts found for %s", username)
	}

	return account, nil
}

// Get all accounts
func (s *PostgresStore) GetAccounts() ([]*common.Account, error) {
	query := `SELECT * FROM accounts`
	rows, err := s.db.Query(query)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	accounts := []*common.Account{}
	for rows.Next() {
		account, err := scanIntoAccount(rows)
		if err != nil {
			return nil, err
		}

		accounts = append(accounts, account)
	}

	return accounts, nil
}

func (s *PostgresStore) UpdateGameAmount(username string) error {
	query := `SELECT count(*) FROM games WHERE owner_name = $1`

	row, err := s.db.Query(query, username)
	if err != nil {
		return err
	}

	defer row.Close()

	row.Next()
	var count int
	err = row.Scan(&count)
	if err != nil {
		return err
	}

	query = `UPDATE accounts SET owned_games = $1 WHERE username = $2`
	_, err = s.db.Exec(query, count, username)
	if err != nil {
		return err
	}

	return nil
}

// Find an account
func scanIntoAccount(rows *sql.Rows) (*common.Account, error) {
	account := new(common.Account)
	err := rows.Scan(
		&account.ID,
		&account.Username,
		&account.Firstname,
		&account.Lastname,
		&account.EncryptedPassword,
		&account.Admin,
		&account.CreatedAt,
		&account.OwnedGames,
	)

	return account, err
}
