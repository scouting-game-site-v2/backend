package storage

import (
	"ScoutingGameSiteV2_Backend/logger"
	"database/sql"
	"os"

	_ "github.com/lib/pq"
)

// Connect to Postgres database
func NewPostgresStore() (*PostgresStore, error) {
	host, found := os.LookupEnv("POSTGRES_HOST")
	if !found {
		host = "postgres"
	}

	port, found := os.LookupEnv("POSTGRES_PORT")
	if !found {
		port = "5432"
	}

	dbname, found := os.LookupEnv("POSTGRES_DB")
	if !found {
		dbname = "gamesite"
	}
	connStr := "user=" + os.Getenv("POSTGRES_USERNAME") + " password=" + os.Getenv("POSTGRES_PASSWORD") + " dbname=" + dbname + " sslmode=disable host=" + host + " port=" + port
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	logger.Info("Connected to Postgres database")

	return &PostgresStore{
		db: db,
	}, nil
}

// Initialize the database
func (s *PostgresStore) Init() error {
	err := s.CreateAccountTable()
	if err != nil {
		return err
	}

	err = s.CreateGameTable()
	if err != nil {
		return err
	}

	err = s.CreateGameLocationsTable()
	if err != nil {
		return err
	}

	return nil
}
