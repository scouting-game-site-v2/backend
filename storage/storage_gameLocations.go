package storage

import (
	"ScoutingGameSiteV2_Backend/common"
	"database/sql"
	"encoding/json"
	"fmt"
)

// Create the game table
func (s *PostgresStore) CreateGameLocationsTable() error {
	// games table contains the following columns:
	// - id = unique id for each location
	// - game_id = the game id where the location belongs to
	// - latitude
	// - longitude
	// - radius = radius of the location
	// - owner = the person where the location belongs to
	// - color = the color of the location
	// - settings = settings for the location, sored as a json object
	query := `CREATE TABLE IF NOT EXISTS game_locations (
		id serial PRIMARY KEY,
		game_id integer NOT NULL REFERENCES games(id),
		latitude float(32) NOT NULL,
		longitude float(32) NOT NULL,
		radius integer NOT NULL,
		owner varchar(100),
		color varchar(10),
		settings json
	)`
	_, err := s.db.Exec(query)
	return err
}

func (s *PostgresStore) CreateGameLocation(location *common.GameLocations) (int, error) {
	lastInsertId := 0
	settingsJSON, err := json.Marshal(location.Settings)
	if err != nil {
		return lastInsertId, err
	}

	query := `INSERT INTO game_locations
		(game_id, latitude, longitude, radius, color, settings)
		VALUES
		($1, $2, $3, $4, $5, $6)
		RETURNING id
	`

	rows, err := s.db.Query(
		query,
		location.Game_id,
		location.Latitude,
		location.Longitude,
		location.Radius,
		location.Color,
		settingsJSON)

	if err != nil {
		return lastInsertId, err
	}

	defer rows.Close()

	rows.Next()
	err = rows.Scan(&lastInsertId)
	if err != nil {
		return lastInsertId, err
	}

	if err != nil {
		return lastInsertId, err
	}
	return lastInsertId, nil
}

// Update a location
func (s *PostgresStore) UpdateGameLocation(location *common.GameLocations) error {
	settingsJSON, err := json.Marshal(location.Settings)
	if err != nil {
		return err
	}

	// update only the latitude, longitude, radius, owner, color, settings
	query := `UPDATE game_locations SET
			latitude = $1,
			longitude = $2,
			radius = $3,
			owner = $4,
			color = $5,
			settings = $6
			WHERE id = $7
		`
	_, err = s.db.Exec(
		query,
		location.Latitude,
		location.Longitude,
		location.Radius,
		location.Owner,
		location.Color,
		settingsJSON,
		location.ID)

	if err != nil {
		return err
	}
	return nil
}

// Delete a location
func (s *PostgresStore) DeleteGameLocation(id int) error {
	// look for the location
	_, getErr := s.GetGameLocationByID(id)
	if getErr != nil {
		return getErr
	}

	// delete the location
	query := `DELETE FROM game_locations WHERE id = $1`
	_, err := s.db.Exec(query, id)

	if err != nil {
		return err
	}
	return nil
}

// Get a location by id
func (s *PostgresStore) GetGameLocationByID(id int) (*common.GameLocations, error) {
	query := `SELECT * FROM game_locations WHERE id = $1`
	rows, err := s.db.Query(query, id)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	if !rows.Next() {
		return nil, fmt.Errorf("location not found for %d", id)
	}

	location, err := scanIntoGameLocations(rows)
	if err != nil {
		return nil, err
	}

	if rows.Next() {
		return nil, fmt.Errorf("multiple locations found for %d", id)
	}

	return location, nil
}

// Get all locations for a game
func (s *PostgresStore) GetGameLocationsByGameID(gameID int) ([]*common.GameLocations, error) {
	query := `SELECT * FROM game_locations WHERE game_id = $1`
	rows, err := s.db.Query(query, gameID)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	// get the locations
	locations := []*common.GameLocations{}
	for rows.Next() {
		location, err := scanIntoGameLocations(rows)

		if err != nil {
			return nil, err
		}
		locations = append(locations, location)
	}

	return locations, nil
}

// Scan into all game locations
func scanIntoGameLocations(rows *sql.Rows) (*common.GameLocations, error) {
	settings := []byte{}
	owner := sql.NullString{}
	color := sql.NullString{}

	location := &common.GameLocations{}
	err := rows.Scan(
		&location.ID,
		&location.Game_id,
		&location.Latitude,
		&location.Longitude,
		&location.Radius,
		&owner,
		&color,
		&settings)

	if err != nil {
		return nil, err
	}

	// unmarshal the settings
	err = json.Unmarshal(settings, &location.Settings)
	if err != nil {
		return nil, err
	}

	// set the owner
	if owner.Valid {
		location.Owner = owner.String
	}

	// set the color
	if color.Valid {
		location.Color = color.String
	}

	return location, err
}
