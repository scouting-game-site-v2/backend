package storage

import (
	"ScoutingGameSiteV2_Backend/common"
	"database/sql"
	"encoding/json"
	"fmt"
	"time"
)

// Create the game table
func (s *PostgresStore) CreateGameTable() error {
	// games table contains the following columns:
	// - id = unique id for each game
	// - name = name of the game
	// - game_type = type of game, part of the enum
	// - active_users = json object containing the users currently playing the game
	// - date_edit = date the game was last edited
	// - owner_name = username of the user who created the game, links to the user table
	// - settings = settings for the game, stored as a json object
	query := `CREATE TABLE IF NOT EXISTS games (
		id serial PRIMARY KEY,
		name varchar(100) NOT NULL,
		game_type integer NOT NULL,
		active_users json,
		created_at timestamp NOT NULL,
		date_edit timestamp NOT NULL,
		settings json,
		owner_name varchar(100) NOT NULL REFERENCES accounts(username)
	)`
	_, err := s.db.Exec(query)
	return err
}

// Create a game
func (s *PostgresStore) CreateGame(game *common.Game) (int, error) {
	lastInsertId := 0
	settingsJSON, err := json.Marshal(game.Settings)
	if err != nil {
		return lastInsertId, err
	}

	activeUsersJSON, err := json.Marshal(game.ActiveUsers)
	if err != nil {
		return lastInsertId, err
	}

	query := `INSERT INTO games 
		(name, game_type, active_users, created_at, date_edit, settings, owner_name)
		VALUES 
		($1, $2, $3, $4, $5, $6, $7)
		RETURNING id
	`

	rows, err := s.db.Query(
		query,
		game.Name,
		game.GameType,
		activeUsersJSON,
		game.CreatedAt,
		game.DateEdit,
		settingsJSON,
		game.OwnerName)

	if err != nil {
		return lastInsertId, err
	}

	defer rows.Close()

	rows.Next()
	err = rows.Scan(&lastInsertId)
	if err != nil {
		return lastInsertId, err
	}

	err = s.UpdateGameAmount(game.OwnerName)
	if err != nil {
		return lastInsertId, err
	}

	return lastInsertId, nil
}

// Update a game
func (s *PostgresStore) UpdateGame(game *common.Game) error {
	// convert the settings to a json object
	settingsJSON, err := json.Marshal(game.Settings)
	if err != nil {
		return err
	}

	activeUsersJSON, err := json.Marshal(game.ActiveUsers)
	if err != nil {
		return err
	}

	// update only the name, game_type, active_users, date_edit, settings
	query := `UPDATE games SET 
			name = $1,
			active_users = $2,
			date_edit = $3,
			settings = $4
			WHERE id = $5
		`
	_, err = s.db.Exec(
		query,
		game.Name,
		activeUsersJSON,
		game.DateEdit,
		settingsJSON,
		game.ID)

	if err != nil {
		return err
	}
	return nil
}

// Copy game from one user to another
func (s *PostgresStore) CopyGame(game *common.Game, newOwnerName string) error {
	// convert the settings to a json object
	settingsJSON, err := json.Marshal(game.Settings)
	if err != nil {
		return err
	}

	activeUsersJSON, err := json.Marshal(game.ActiveUsers)
	if err != nil {
		return err
	}

	query := `INSERT INTO games 
		(name, game_type, created_at, active_users, date_edit, settings, owner_name)
		VALUES 
		($1, $2, $3, $4, $5, $6, $7)
		RETURNING ID
	`
	_, err = s.db.Exec(
		query,
		game.Name,
		game.GameType,
		activeUsersJSON,
		game.CreatedAt,
		game.DateEdit,
		settingsJSON,
		newOwnerName)

	if err != nil {
		return err
	}

	err = s.UpdateGameAmount(newOwnerName)
	if err != nil {
		return err
	}

	return nil
}

// Delete a game
func (s *PostgresStore) DeleteGame(id int) error {
	// look for the game
	game, getErr := s.GetGameByID(id)
	if getErr != nil {
		return getErr
	}

	query := `DELETE FROM games WHERE id = $1`
	_, err := s.db.Exec(query, id)

	if err != nil {
		return err
	}

	err = s.UpdateGameAmount(game.OwnerName)
	if err != nil {
		return err
	}

	return nil
}

// Get a game by id
func (s *PostgresStore) GetGameByID(id int) (*common.Game, error) {
	query := `SELECT * FROM games WHERE id = $1`
	rows, err := s.db.Query(query, id)
	if err != nil {

		return nil, err
	}

	defer rows.Close()

	if !rows.Next() {
		return nil, fmt.Errorf("game with id %d not found", id)
	}

	game, err := scanIntoGames(rows)
	if err != nil {
		return nil, err
	}

	if rows.Next() {
		return nil, fmt.Errorf("multiple games with id %d", id)
	}

	return game, nil
}

func (s *PostgresStore) GetGamesByOwnerName(ownerName string) ([]*common.Game, error) {
	query := `SELECT * FROM games WHERE owner_name = $1`
	rows, err := s.db.Query(query, ownerName)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	games := []*common.Game{}
	for rows.Next() {
		game, err := scanIntoGames(rows)

		if err != nil {
			return nil, err
		}

		games = append(games, game)
	}

	return games, nil
}

// Scan into a games
func scanIntoGames(rows *sql.Rows) (*common.Game, error) {
	activeUsers := []byte{}
	settings := []byte{}

	game := new(common.Game)
	err := rows.Scan(
		&game.ID,
		&game.Name,
		&game.GameType,
		&activeUsers,
		&game.CreatedAt,
		&game.DateEdit,
		&settings,
		&game.OwnerName,
	)

	if err != nil {
		return nil, err
	}

	// TODO: replace with len(storage.ActiveUsers[game_id])

	// err = json.Unmarshal(activeUsers, &game.ActiveUsers)
	// if err != nil {
	// 	return nil, err
	// }

	err = json.Unmarshal(settings, &game.Settings)
	if err != nil {
		return nil, err
	}

	return game, err
}

// Update the date_edit of a game
func (s *PostgresStore) UpdateGameEditDate(gameID int) error {
	dateNow := time.Now().UTC()

	query := `UPDATE games SET date_edit = $1 WHERE id = $2`
	_, err := s.db.Exec(query, dateNow, gameID)

	if err != nil {
		return err
	}

	return nil
}
