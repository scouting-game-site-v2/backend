package storage

import (
	"ScoutingGameSiteV2_Backend/common"
	"database/sql"

	"github.com/gorilla/websocket"
)

type ActiveUser struct {
	GameUsername string          `json:"game_username"`
	Color        string          `json:"color"`
	Conn         *websocket.Conn `json:"conn"`
}

var ActiveUsers = make(map[int][]ActiveUser)

type AccountStorage interface {
	CreateAccount(*common.Account) (int, error)
	DeleteAccount(string) error
	UpdateAccount(*common.Account) error
	GetAccounts() ([]*common.Account, error)
	GetAccountByUsername(string) (*common.Account, error)
}

type GameStorage interface {
	CreateGame(*common.Game) (int, error)
	DeleteGame(int) error
	UpdateGame(*common.Game) error
	CopyGame(*common.Game, string) error
	GetGameByID(int) (*common.Game, error)
	GetGamesByOwnerName(string) ([]*common.Game, error)
	UpdateGameEditDate(int) error
}
type GameLocationStorage interface {
	CreateGameLocation(*common.GameLocations) (int, error)
	UpdateGameLocation(*common.GameLocations) error
	DeleteGameLocation(int) error
	GetGameLocationByID(int) (*common.GameLocations, error)
	GetGameLocationsByGameID(int) ([]*common.GameLocations, error)
}

type PostgresStore struct {
	db *sql.DB
}
